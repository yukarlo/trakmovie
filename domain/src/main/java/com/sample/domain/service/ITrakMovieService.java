package com.sample.domain.service;


import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;

import java.util.List;

import rx.Observable;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public interface ITrakMovieService {
	Observable<List<Movie>> popularObservable(int page);

	Observable<List<SearchMovie>> queryObservable(String query, int page);
}
