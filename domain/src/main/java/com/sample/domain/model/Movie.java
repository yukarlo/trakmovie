package com.sample.domain.model;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public class Movie {
	private String title;
	private String overview;
	private Integer year;
	private Images images;
	private Float rating;

	public Movie (String title, String overview, Integer year, Images images, Float rating) {
		this.title = title;
		this.overview = overview;
		this.year = year;
		this.images = images;
		this.rating = rating;
	}

	public String getTitle() {
		return title;
	}

	public String getOverview() {
		return overview;
	}

	public Integer getYear() {
		return year;
	}

	public Images getImages() {
		return images;
	}

	public Float getRating() {
		return rating;
	}
}
