package com.sample.domain.model;

/**
 * Created by Karlo Miguel P. Yu on 7/21/2016.
 */

public class Fanart {
    public String full;
    public String medium;
    public String thumb;

    public String getFull() {
        return full;
    }

    public String getMedium() {
        return medium;
    }

    public String getThumb() {
        return thumb;
    }
}
