package com.sample.domain.model;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public class Poster {
    private String full;
    private String medium;
    private String thumb;

    public String getFull() {
        return full;
    }

    public String getMedium() {
        return medium;
    }

    public String getThumb() {
        return thumb;
    }
}
