package com.sample.domain;

import com.sample.domain.model.Movie;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Karlo Miguel P. Yu on 8/11/2016.
 */

public class MovieTest {

    private Movie mMovie;

    @Before
    public void setUp() {
        mMovie = TestData.createFakeMovie("Sample");
    }

    @Test
    public void testUserConstructorHappyCase() {
        String title = mMovie.getTitle();
        assertThat(title, is("Sample"));
    }
}
