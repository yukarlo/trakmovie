package com.sample.domain;

import com.sample.domain.model.Movie;

/**
 * Created by Karlo Miguel P. Yu on 8/11/2016.
 */

public class TestData {

    public static Movie createFakeMovie (String sample) {
        return new Movie(sample, sample, sample.length(), null, Float.valueOf(sample.length()));
    }
}
