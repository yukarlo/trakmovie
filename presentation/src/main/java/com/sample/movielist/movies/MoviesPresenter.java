package com.sample.movielist.movies;

import android.content.Context;

import com.sample.data.service.TrakMovieService;
import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;
import com.sample.domain.service.ITrakMovieService;
import com.sample.movielist.base.BasePresenter;
import com.sample.movielist.util.NetworkUtil;

import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public class MoviesPresenter extends BasePresenter<IMoviesView> {

    private ITrakMovieService mITrakMovie;
    private Subscription mQuerySubscription;
    private boolean isLoadingMovies = false;
    private int page = 1;
    private boolean isLastPage = false;
    private List<Movie> mMovies;

    public MoviesPresenter (TrakMovieService movieService) {
        mITrakMovie = movieService;
    }

    @Override
    public void attachView (IMoviesView iMoviesView) {
        super.attachView(iMoviesView);
    }

    @Override
    public void detachView () {
        super.detachView();
    }

    public void getPopularMoviesObservable (Context context) {
        if (NetworkUtil.isNetworkConnected(context)) {
            isLoadingMovies = true;
            getMvpView().showProgress(true);
            Observable<List<Movie>> popularMovies = mITrakMovie.popularObservable(page);
            popularMovies.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getPopularMovieSubscriber());
        } else {
            getMvpView().showError(0);
            getMvpView().showProgress(false);
        }
    }

    public boolean isLoadingMovies () {
        return isLoadingMovies;
    }

    public Subscriber<List<Movie>> getPopularMovieSubscriber () {
        return new Subscriber<List<Movie>>() {
            @Override
            public void onCompleted () {

            }

            @Override
            public void onError (Throwable e) {
                isLoadingMovies = false;
                getMvpView().showProgress(false);
                if (e instanceof HttpException)
                    getMvpView().showError(((HttpException) e).code());
            }

            @Override
            public void onNext (List<Movie> movies) {
                mMovies = movies;
                page++;
                isLoadingMovies = false;
                getMvpView().showProgress(false);
                getMvpView().populatePopular(movies);
            }
        };
    }

    public List<Movie> getMovies () {
        return mMovies;
    }

    public Subscriber<List<SearchMovie>> getSearchedMovieSubscriber () {
        return new Subscriber<List<SearchMovie>>() {
            @Override
            public void onCompleted () {

            }

            @Override
            public void onError (Throwable e) {

                getMvpView().showProgress(false);
                if (e instanceof HttpException)
                    getMvpView().showError(((HttpException) e).code());
            }

            @Override
            public void onNext (List<SearchMovie> movies) {
                if (movies.size() <= 0) {
                    isLastPage = true;
                }
                page++;
                getMvpView().showProgress(false);
                getMvpView().populateQuery(movies);
            }
        };
    }

    public void queryMovies (String query, Context context) {
        if (NetworkUtil.isNetworkConnected(context)) {
            getMvpView().showProgress(true);
            if (mQuerySubscription != null) {
                cancelQuery();
            }
            Observable<List<SearchMovie>> queryMovies = mITrakMovie.queryObservable(query, page);
            mQuerySubscription = queryMovies.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getSearchedMovieSubscriber());
        } else {
            getMvpView().showError(0);
            getMvpView().showProgress(false);
        }
    }

    public boolean isLastPage () {
        return isLastPage;
    }

    public void cancelQuery () {
        if (mQuerySubscription != null)
            mQuerySubscription.unsubscribe();
    }

    public void resetPage () {
        isLastPage = false;
        page = 1;
    }
}
