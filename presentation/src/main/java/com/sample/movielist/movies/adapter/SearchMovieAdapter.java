package com.sample.movielist.movies.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;
import com.sample.movielist.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Karlo Miguel P. Yu on 7/21/2016.
 */

public class SearchMovieAdapter extends RecyclerView.Adapter<SearchMovieAdapter.ViewHolder> {

    private final List<SearchMovie> mMovies;
    private final View mEmptyView;
    private Context mContext;

    public SearchMovieAdapter(Context context, View emptyView) {
        super();
        mContext = context;
        mEmptyView = emptyView;
        mMovies = new ArrayList<>();
    }

    public void addAll(List<SearchMovie> movies) {
        mMovies.addAll(movies);
        notifyDataSetChanged();
        mEmptyView.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_query_list_template, parent, false);
        return new SearchMovieAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SearchMovie searchMovie = getItemAtPosition(position);
        Movie movie = searchMovie.getMovie();
        if (movie.getImages() != null) {
            String movieImageUrl = movie.getImages().poster.getThumb();
            Picasso.with(mContext).load(movieImageUrl)
                    .placeholder(R.drawable.placeholder_poster)
                    .error(R.drawable.placeholder_poster)
                    .into(holder.mMoviePosterImageView);
        }

        String overview = movie.getOverview();
        if (overview == "" || overview == null) {
            overview = "No movie overview available.";
        }
        holder.mMovieOverviewTextView.setText(overview);

        String title = movie.getTitle();
        if (movie.getYear() != null) {
            String year = movie.getYear().toString();
            title = title + " (" + year + ")";
        }
        holder.mMovieTitleTextView.setText(title);
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    private SearchMovie getItemAtPosition(int position) {
        return mMovies.get(position);
    }

    public void clear() {
        mMovies.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.search_movie_poster_image_view)
        ImageView mMoviePosterImageView;
        @BindView(R.id.search_movie_title_text_view)
        TextView mMovieTitleTextView;
        @BindView(R.id.search_movie_overview_text_view)
        TextView mMovieOverviewTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
