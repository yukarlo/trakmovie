package com.sample.movielist.movies;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sample.data.service.TrakMovieService;
import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;
import com.sample.movielist.R;
import com.sample.movielist.base.BaseActivity;
import com.sample.movielist.movies.adapter.MovieAdapter;
import com.sample.movielist.movies.adapter.SearchMovieAdapter;
import com.sample.movielist.util.NetworkUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public class MoviesActivity extends BaseActivity implements IMoviesView,
        SearchView.OnQueryTextListener,
        MenuItemCompat.OnActionExpandListener {
    @BindView (R.id.movie_recycler_view)
    RecyclerView mMovieRecyclerView;
    @BindView (R.id.empty_movie_grid_text_view)
    TextView mEmptyMovieTextView;
    @BindView (R.id.progress_on_first_load)
    ProgressBar mProgressOnFirstLoad;
    @BindView (R.id.progress)
    ProgressBar mProgressBar;

    private MovieAdapter mMovieAdapter;
    private SearchMovieAdapter mSearchMovieAdapter;
    private MoviesPresenter mMoviesPresenter;
    private String queryText;
    private GridLayoutManager mGridLayoutManager;
    private LinearLayoutManager mLinearLayoutManager;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        mMoviesPresenter = new MoviesPresenter(new TrakMovieService());
        mMoviesPresenter.attachView(this);

        setUpPopularMovies();

        mMovieRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled (RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    if (mMoviesPresenter.isLoadingMovies() || !hasReachedBottomOfList()) {
                        return;
                    }
                    if (queryText != null) {
                        if (!mMoviesPresenter.isLastPage()) {
                            mMoviesPresenter.queryMovies(queryText, MoviesActivity.this);
                        }
                    } else {
                        mMoviesPresenter.getPopularMoviesObservable(MoviesActivity.this);
                    }
                }
            }
        });

        mMoviesPresenter.getPopularMoviesObservable(MoviesActivity.this);
    }

    private boolean hasReachedBottomOfList () {
        int visibleItemCount;
        int pastVisibleItems;
        int totalItemCount;
        if (mGridLayoutManager != null) {
            visibleItemCount = mGridLayoutManager.getChildCount();
            pastVisibleItems = mGridLayoutManager.findFirstVisibleItemPosition();
            totalItemCount = mMovieAdapter.getItemCount();
        } else {
            visibleItemCount = mLinearLayoutManager.getChildCount();
            pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
            totalItemCount = mSearchMovieAdapter.getItemCount();
        }
        return (visibleItemCount + pastVisibleItems) >= totalItemCount;
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem actionMenuSearchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) actionMenuSearchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(actionMenuSearchItem, this);
        return true;
    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
        mMoviesPresenter.detachView();
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cancelSearch () {
        queryText = null;
        mMoviesPresenter.cancelQuery();
        mMoviesPresenter.getPopularMoviesObservable(MoviesActivity.this);
    }

    @Override
    public void populatePopular (List<Movie> movies) {
        mMovieAdapter.addAll(movies);
    }

    @Override
    public void populateQuery (List<SearchMovie> movies) {
        if (movies.isEmpty()) {
            mEmptyMovieTextView.setText("No movies found");
            mEmptyMovieTextView.setVisibility(View.VISIBLE);
        } else {
            mSearchMovieAdapter.addAll(movies);
        }
    }

    @Override
    public void showError (int code) {
        String message = NetworkUtil.getErrorMessage(code);
        mEmptyMovieTextView.setText(message);
        mEmptyMovieTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress (boolean show) {
        if (show) {
            if (mGridLayoutManager == null) {
                int searchMovieItemCount = mSearchMovieAdapter.getItemCount();
                if (searchMovieItemCount == 0) {
                    mProgressOnFirstLoad.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                } else
                    mProgressBar.setVisibility(View.VISIBLE);
            } else {
                int popularMovieCount = mMovieAdapter.getItemCount();
                if (popularMovieCount == 0)
                    mProgressBar.setVisibility(View.GONE);
                else
                    mProgressBar.setVisibility(View.VISIBLE);
            }
        } else {
            mProgressOnFirstLoad.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onQueryTextSubmit (String query) {
        mEmptyMovieTextView.setVisibility(View.GONE);
        if (query.length() > 0) {
            queryText = query;
            mMoviesPresenter.queryMovies(query, MoviesActivity.this);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange (String query) {
        mEmptyMovieTextView.setVisibility(View.GONE);
        mMoviesPresenter.resetPage();
        mSearchMovieAdapter.clear();
        if (query.length() > 0) {
            queryText = query;
            mMoviesPresenter.queryMovies(query, MoviesActivity.this);
        } else {
            queryText = null;
            showProgress(false);
            mMoviesPresenter.cancelQuery();
        }

        return false;
    }

    @Override
    public boolean onMenuItemActionExpand (MenuItem item) {
        mEmptyMovieTextView.setVisibility(View.GONE);
        mGridLayoutManager = null;
        mSearchMovieAdapter = new SearchMovieAdapter(this, mEmptyMovieTextView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mMovieRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMovieRecyclerView.setHasFixedSize(true);
        mMovieRecyclerView.setAdapter(mSearchMovieAdapter);

        mMoviesPresenter.resetPage();
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse (MenuItem item) {
        setUpPopularMovies();
        cancelSearch();
        return true;
    }

    private void setUpPopularMovies () {
        mLinearLayoutManager = null;
        mMovieAdapter = new MovieAdapter(this, mEmptyMovieTextView);
        mGridLayoutManager = new GridLayoutManager(this, 2);
        mMovieRecyclerView.setLayoutManager(mGridLayoutManager);
        mMovieRecyclerView.setHasFixedSize(true);
        mMovieRecyclerView.setAdapter(mMovieAdapter);

        mMoviesPresenter.resetPage();
    }
}
