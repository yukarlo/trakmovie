package com.sample.movielist.movies;

import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;
import com.sample.movielist.base.IMvpView;

import java.util.List;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public interface IMoviesView extends IMvpView {
	void populatePopular(List<Movie> movies);

	void populateQuery(List<SearchMovie> movies);

	void showError(int code);

    void showProgress(boolean show);
}
