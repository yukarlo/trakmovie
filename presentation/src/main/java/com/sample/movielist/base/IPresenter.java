package com.sample.movielist.base;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public interface IPresenter<V extends IMvpView> {

    void attachView(V mvpView);

    void detachView();
}