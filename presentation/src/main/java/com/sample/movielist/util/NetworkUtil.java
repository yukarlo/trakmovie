package com.sample.movielist.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Karlo Miguel P. Yu on 7/23/2016.
 */
public class NetworkUtil {

	public static boolean isNetworkConnected(Context context) {
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}

	public static String getErrorMessage(int code) {
		String message;
		switch (code) {
			case 403:
			case 400:
				message = "There was an error in loading data";
				break;
			case 404:
				message = "No record found";
				break;
			case 500:
				message = "Server error.";
				break;
			case 503:
			case 504:
			case 520:
			case 521:
			case 522:
				message = "Service unavailable";
				break;
			default:
				message = "The network is not available. Please check your internet connection.";
				break;
		}
		return message;
	}

}
