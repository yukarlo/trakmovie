package com.sample.data;

import com.sample.data.service.TrakMovieService;
import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;

import org.junit.Test;

import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Karlo Miguel P. Yu on 7/22/2016.
 */

public class MovieTest {

	@Test
	public void getPopularMoviesShouldReturnTenItems() throws Exception {
		TrakMovieService trakMovieService = new TrakMovieService();
		Observable<List<Movie>> listObservable = trakMovieService.popularObservable(1);
		TestSubscriber<List<Movie>> testSubscriber = new TestSubscriber<>();
		listObservable.subscribe(testSubscriber);

		testSubscriber.assertNoErrors();
		List<Movie> movies = testSubscriber.getOnNextEvents().get(0);
		assertEquals(10, movies.size());
	}

	@Test
	public void getSearchMoviesShouldReturnTitleThatContainsQuery() throws Exception {
		TrakMovieService trakMovieService = new TrakMovieService();
		Observable<List<SearchMovie>> listObservable = trakMovieService.queryObservable("Tron", 1);
		TestSubscriber<List<SearchMovie>> testSubscriber = new TestSubscriber<>();
		listObservable.subscribe(testSubscriber);

		testSubscriber.assertNoErrors();
		List<SearchMovie> movies = testSubscriber.getOnNextEvents().get(0);
		assertNotNull(movies);
		String title = movies.get(0).getMovie().getTitle();
		assertTrue(title.toLowerCase().contains("tron"));
	}
}
