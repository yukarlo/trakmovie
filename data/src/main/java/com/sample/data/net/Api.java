package com.sample.data.net;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public class Api {

	public static final String BASE_URL = "https://api.trakt.tv/";

	private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

	private static Retrofit.Builder builder =
			new Retrofit.Builder()
					.baseUrl(BASE_URL)
					.addConverterFactory(GsonConverterFactory.create());

	public static <S> S createService(Class<S> serviceClass) {
		OkHttpClient client = httpClient.build();
		Retrofit retrofit = builder
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.client(client).build();
		return retrofit.create(serviceClass);
	}
}
