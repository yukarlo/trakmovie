package com.sample.data.net;

import com.sample.data.BuildConfig;
import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public interface IApi {

	String VALUE_VERSION = "2";
	String VALUE_CONTENT_TYPE = "application/json";

	String HEADER_PARAM_CONTENT_TYPE = "Content-Type";
	String HEADER_PARAM_API_VERSION = "trakt-api-version";
	String HEADER_PARAM_API_KEY = "trakt-api-key";

	String PARAM_QUERY = "query";
	String PARAM_TYPE = "type";
	String PARAM_PAGE = "page";
	String PARAM_LIMIT = "limit";
	String PARAM_EXTENDED = "extended";

	@Headers({
			HEADER_PARAM_CONTENT_TYPE + " : " + VALUE_CONTENT_TYPE,
			HEADER_PARAM_API_VERSION + ": " + VALUE_VERSION,
			HEADER_PARAM_API_KEY + ": " + BuildConfig.TRAK_API_KEY
	})
	@GET("movies/popular")
	Observable<List<Movie>> popularObservable(@Query(PARAM_PAGE) int page,
											  @Query(PARAM_LIMIT) int limit,
											  @Query(PARAM_EXTENDED) String extension);


	@Headers({
			HEADER_PARAM_CONTENT_TYPE + ":" + VALUE_CONTENT_TYPE,
			HEADER_PARAM_API_VERSION + ": " + VALUE_VERSION,
			HEADER_PARAM_API_KEY + ": " + BuildConfig.TRAK_API_KEY
	})
	@GET("search")
	Observable<List<SearchMovie>> queryMovies(@Query(PARAM_QUERY) String query,
                                              @Query(PARAM_TYPE) String type,
                                              @Query(PARAM_PAGE) int page,
                                              @Query(PARAM_LIMIT) int limit,
                                              @Query(PARAM_EXTENDED) String extension);
}
