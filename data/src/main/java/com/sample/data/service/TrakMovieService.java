package com.sample.data.service;

import com.sample.data.net.Api;
import com.sample.data.net.IApi;
import com.sample.domain.model.Movie;
import com.sample.domain.model.SearchMovie;
import com.sample.domain.service.ITrakMovieService;

import java.util.List;

import rx.Observable;

/**
 * Created by Karlo Miguel P. Yu on 7/20/2016.
 */

public class TrakMovieService implements ITrakMovieService {

	private static final String TYPE_MOVIE = "movie";
	private static final String EXTENSION = "full,images";
	private static final int PAGE_SIZE = 10;
	private IApi mIApi;

	public TrakMovieService() {
        mIApi = Api.createService(IApi.class);
	}

	@Override
	public Observable<List<Movie>> popularObservable(int page) {
		return mIApi.popularObservable(page, PAGE_SIZE, EXTENSION);
	}

	@Override
	public Observable<List<SearchMovie>> queryObservable(String query, int page) {
		return mIApi.queryMovies(query, TYPE_MOVIE, page, PAGE_SIZE, EXTENSION);
	}
}
